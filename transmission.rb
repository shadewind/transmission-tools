require "uri"
require "net/http"
require "psych"
require "base64"

# Thrown by the +Transmission+ class.
class TransmissionError < StandardError
end

# Creates a Transmission object with settings loaded from
# ~/.transmission-rpc or using default url http://localhost:9091/transmission/rpc
def make_transmission
    begin
        config = Psych.load(File.new("#{Dir.home}/.transmission-rpc"))
        return Transmission.new(config["uri"],
            username: config["username"],
            password: config["password"]
        )
    rescue Errno::ENOENT
        return Transmission.new("http://localhost:9091/transmission/rpc")
    end
end

# Transmission RPC client.
class Transmission
    # The request URI
    attr_reader :uri
    # The username.
    attr_accessor :username
    # The password.
    attr_accessor :password

    # Creates a new Transmission RPC client.
    def initialize(uri, username: nil, password: nil)
        @uri = uri.is_a?(URI) ? uri : URI(uri)
        @username = username
        @password = password
        @http = Net::HTTP.new(@uri.host, @uri.port)
        @http.read_timeout = 3
    end

    # Makes a request.
    def do_request(method, args, tag = nil)
        response = nil
        begin
            request = Net::HTTP::Post.new(@uri.path)
            request.basic_auth(@username, @password) unless @username.nil?
            data = { method: method, arguments: args }
            data[:tag] = tag unless tag.nil?
            request.body = JSON.generate(data)
            request["X-Transmission-Session-Id"] = @session_id unless @session_id.nil?

            response = @http.request(request)
            response.value

        rescue Net::HTTPServerException => e
            if e.data.is_a?(Net::HTTPConflict)
                @session_id = response["X-Transmission-Session-Id"]
                retry
            end
            raise
        end

        JSON.parse(response.body)
    end

    # Adds a download for the specified URI. The torrent will be downloaded to +path+
    # or to wherever Transmission feels like if +nil+.
    def download_uri(uri, download_dir: nil)
        args = { "filename" => uri }
        args["download-dir"] = download_dir unless download_dir.nil?
        do_request("torrent-add", args)
    end

    # Adds a download for the specified torrent data. The torrent will be downloaded to +path+
    # or to wherever Transmission feels like if +nil+.
    def download_torrent(data, download_dir: nil)
        args = { "metainfo" => Base64.encode64(data) }
        args["download-dir"] = download_dir unless download_dir.nil?
        do_request("torrent-add", args)
    end
end